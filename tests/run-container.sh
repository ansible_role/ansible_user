#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${DIR}

docker ps -a | grep ansible_user_test
IMAGE_EXISTS=$?
if [ ${IMAGE_EXISTS} -ne "0" ];
then
    docker build -t ansible_user_test .
fi

ssh-keygen -q -f ~/.ssh/known_hosts -R [localhost]:7922
docker run --name ansible_user_test -p 7922:22 --rm -it ansible_user_test