## Ansible role for OS user management.

This role allows to create users and there groups.
Passwords of users can be defined as plaintext, they will be hashed on the fly.
